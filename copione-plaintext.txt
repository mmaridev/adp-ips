


I promessi sposi: oggi

Area progetto: II D



Prof.  ssa Angela Rosato



Il cortometraggio è stato tratto dal romanzo “ I promessi sposi” di Alessandro Manzoni, 1840.











Sequenze del film:
1. Titoli di testa e presentazione del narratore
2. Matrimonio Renzo e Lucia
3. Don Abbondio e i bravi
4. Don Abbondio e Perpetua
5. Lucia, Agnese  e don Abbondio
6. Colloquio Lucia e fra’ Cristoforo
7. Fuga dal paese di Lucia, Renzo e Agnese
8. Arrivo in città: Lucia si reca al convento da suor Gertrude, Renzo deve recarsi dal cugino Bartolomeo
9. Renzo e la rivolta degli studenti
10. Renzo in fuga raggiunge un cugino
11. Don Rodrigo commissiona all’Innominato il rapimento
12. Rapimento di Lucia
13. Liberazione di Lucia
14.  Renzo e Lucia di nuovo insieme

Scena I: 22 aprile 2017, chiesa
Inquadratura chiesa: interni ed esterni, panoramica sacerdote, invitati, sposo e testimoni
Protagonisti: Lucia, Renzo, testimoni (Tonio e Gervaso), Agnese, Perpetua, Don Abbondio, Innominato, bravi e invitati
Narratore: 
La portella di una macchina che sbatte, il vocio si attenua, la sposa fa il suo ingresso nella Chiesa al braccio dell’Innominato, parte la marcia nuziale e la magia del matrimonio avvolge la chiesa.
(Lucia avanza  lungo la navata, raggiunge Renzo ai piedi dell’altare, il quale l’attende con i suoi testimoni, Tonio e Gervaso. Agnese commossa piange lacrime di gioia. L’ Innominato porge la mano di Lucia a Renzo).
Narratore:
Lucia e Renzo sono dinanzi a Don Abbondio, la funzione ha inizio. Don Abbondio sta per pronunciare la fatidica formula, con la quale finalmente Renzo e Lucia saranno marito e moglie, ma…
Dialoghi:
Don Abbondio (Lunardi): Chi è a conoscenza di qualche impedimento per il quale quest’uomo e questa donna non dovrebbero unirsi in matrimonio, parli ora o taccia per sempre.
Narratore:
A queste parole, un trambusto si leva dal fondo della chiesa, cosa succede? Ecco tre losche figure che avanzano lungo la navata.
(I bravi escono dai banchi posti lungo le navate, e minacciosi avanzano lungo la navata)
Bravo 1 (Ferrarese): ( alza il braccio): Noi!!
Bravo 2 (Gabrielli): Fermi tutti questa è una rapina. 
(Mormorio di paura dei presenti in chiesa)
Bravo 3 (Di Falco): Ma sta’ zitto!! (rivolto al bravo 2) Prete: Questo matrimonio non s’ha da fare, né ora né mai”.

Scena II:
Inquadratura: lago di Carezza, paese Nova Levante.
Protagonista: Don Abbondio e i bravi
Narratore:
Ebbene deve sapere il nostro illustre spettatore, che i fatti, che accingiamo a raccontare risalgono al lontano 2015, quando il prete, fece un inquietante incontro ….
Correva l’anno 2015, il 22 aprile, per la precisione, in una giornata di inizio primavera, il nostro prete come soleva fare ogni mattina , passeggiava per le stradine che costeggiavano il Lago di Carezza: uno specchio d’acqua, che si può definire un gioiello creato dalla natura, incastonato nell’area a ridosso delle Dolomiti, situato in Val d’Ega, a 25 km da Bolzano, nel comune di Nova Levante. Paese in cui si svolgeranno le  vicende da noi raccontate. 
Quando all’improvviso, nei pressi del bivio che conduceva al paese, vide delle figure poco raccomandabili, vestite alla maniera degli scagnozzi di un noto malvivente della malavita sicula, che  aveva trovato rifugio tra quelle pacifiche montagne dell’Alto Adige: Don Rodrigo. Il nostro prete che non era nato con un cuor di leone, alla vista dei due sbiancò, mise le mani nel collare, come raccomandarlo e smarrito volgeva intanto la faccia indietro, torcendo insieme la bocca, e guardando con la coda dell’occhio, fin dove poteva, sperando che quei loschi individui non aspettassero lui, ma….in giro non c’era anima viva. Affrettò il passo, cercando di stamparsi sul viso un sorriso di circostanza.
Dialoghi:
Bravo 3 (Di Falco): Signor parroco.
Don Abbondio: Cosa comanda?
Bravi 2 (Gabrielli): Ci consegni il portafogli!!
Bravo 1 (Ferrarese): [Ancora, con 'sta cosa!!!] Ma taci!! (rivolto al bravo 2) [Se è un flashback l’intervento in chiesa non dovrebbe aver ancora avuto luogo o?]
Bravo 3 (Di Falco): È lei che domani unirà in matrimonio Renzo Tramaglino e Lucia Mondella?
Don Abbondio (Lunardi): cioè…sapete com’è….questi giovani scapestrati, hanno fretta di sposarsi…
Bravo 1 ( Ferrarese): …ma questo matrimonio non sa dà fare né domani né mai…altrimenti ( il bravo con il pollice fa il segno di tagliargli la gola).
Don Abbondio (Lunardi): (impaurito) …ma, signori miei…cercate di capire se dipendesse da me…, a me non vien nulla in tasca..
Bravo 3 (Di Falco): (il bravo con fare minaccioso) Mi raccomando…prete…uomo avvertito…intende vero!!
Scena III:
Inquadratura: casa di Don Abbondio
Protagonista: Don Abbondio e Perpetua
Narratore: 
Don Abbondio sconvolto da  quanto successo, si affretta a ritornare a casa, rimuginando sul da farsi, per di più la nostra Lucia, era una donna dal carattere irascibile, che per un nonnulla prendeva fuoco!!
Intanto la sua Perpetua era intenta a  rassettare la casa ed ogni tanto postava qualche foto su Facebook ed Instagram….e perché no, qualche smorfia su Snapchat!!
Quando fa il suo ingresso, un Don Abbondio sconvolto e  Perpetua si chiede cosa possa mai essergli capitato.
Dialoghi:
Perpetua (Pfeifer): Cosa ti è successo?! Sembra che tu abbia visto un fantasma!!
Don Abbondio: …peggio, peggio!! Delle vipere!!
Perpetua:…ma va là, inventate un’altra!! Parla!!
Don Abbondio: ...ho incontrato il diavolo in persona!!!
Perpetua: …parla benedetto uomo!!!
Narratore: 
Il prete con molta circospezione racconta tutto alla donna…raccomandandole di non far parola con anima viva, ne va…ne va la vita, ripeteva il pover uomo …ma la nostra Perpetua, che ormai era diventata una social woman, immediatamente posta la notizia su Facebook…in quel momento era on line colei alla quale il destino stava per giocarle un brutto scherzo: Lucia.

Scena IV:
Inquadratura: tragitto di Lucia e Agnese, casa di don Abbondio sala da pranzo, pranzo. Telefonata Lucia e fra Cristoforo, Don Rodrigo e i bravi, Attilio ( Don Rodrigo e chiamata a Innominato, da farla in un altro momento), Bartolomeo ( Marinello)
Protagonisti: Lucia, Agnese , Perpetua, Don Abbondio, Renzo, fra’ Cristoforo
Narratore:
Lucia, insieme alla madre Agnese, stava andando a casa di Don Abbondio per gli ultimi preparativi quando la nostra amica sente il segnale acustico che annunciava una nuova notifica di Facebook…Lucia legge la notifica e, infuriata come un toro, affretta il passo per raggiungere la casa del prete e, infuriata come non mai, suona il campanello… nel frattempo il nostro cuor di leone, ignaro della leggerezza commessa da Perpetua, desinava tra mille pensieri…
Don Abbondio: (al suono del campanello) Chi mai osa disturbare il mio pranzo? Perpetua va a vedere, e digli che il parroco oggi non sta bene e non riceve nessuno, e poi questa non è ora per disturbare la brava gente.
(Perpetua va alla porta, apre la porta, non ha il tempo di parlare che viene travolta da una Lucia infuriata, seguita da un ancor più infuriata Agnese)
Lucia (Podini): Prete, che storia è mai questa? Non si fa più il matrimonio? Ma io ti ammazzo…
Don Abbondio (spaventato inizia a parlare in tedesco, con frasi sconnesse)
Narratore:
Deve sapere il nostro spettatore che le nostre due donne, Lucia e Agnese, non erano nate in Alto Adige e non capivano il tedesco e questo fece infuriare ancor di più la nostra protagonista e sua madre che parlavano tutte e due in modo concitato. Ad un certo Lucia iniziò a colpire Don Abbondio mentre Perpetua, non paga dell’enorme caos creato, trasmetteva tutto in diretta su Facebook e la scena veniva guardata proprio da colui che aveva contribuito a creare quel pandemonio: Don Rodrigo.
Lucia: Prete smettila di farneticare cose senza senso e dimmi, chi mai può volere male a me e al mio cucciolotto?
Don Abbondio: …non posso, non posso mi vuoi morto? Non m’ha da premere per la mia vita?
Lucia: parla!!!
Narratore:
…e il nostro prete confessa tutto!! Lucia piangendo e sbraitando lascia la casa. Intanto chiama il suo amico fra’ Cristoforo e si danno appuntamento a casa sua, dove l’attendeva Renzo.
…passati 10 m, ecco che la combriccola si è riunita, e devono decidere sul da farsi…
Fra’ Cristoforo: …miei cari l’unica possibilità è andare via di qua, separarsi per un po’...far calmare le acque e poi celebrare il matrimonio, tu, Lucia, insieme a tua madre, troverete ricovero presso Suor Gertrude. Tu, Renzo, recati da tuo cugino Bartolomeo, a Laives, e cerca di restare il più tranquillo possibile!!
Lucia: maledetto Don Rodrigo!!
Narratore: 
I nostri amici si dirigono velocemente alla prima fermata dell’autobus, che li condurrà a Bolzano. Con l’animo gonfio di tristezza, misto rabbia, guardano con nostalgia il loro paesello, vanno via.
Nel mentre, Don Rodrigo, che aveva appreso la notizia dai social, sorseggiando un caffè con il cugino Attilio (Brazzo) al bar del paese, chiama i suoi scagnozzi, che un  po’ più in là controllano che non capiti nulla di male al loro capo.
Don Rodrigo: Attilio, è fatta, ora mando i bravi a prendere Lucia.
Attilio (Brazzo): …e bravo il mio cuginetto, non se né fa mai scappare una!! ( risata)
Don Rodrigo: (chiama ad alta voce i bravi) Ragazzi, venite qua!!
Bravi all’unisono: Comandi signore!!
Don Rodrigo: Andate a prendere Lucia e portatela qua subito!! Ma badate sopratutto che non le sia fatto male!!
Bravo (Di Falco): Signore… mi perdoni… un po’ di spavento è inevitabile… perché ella non faccia troppo strepitio… non si potrà far di meno.
Don Rodrigo: Spavento…capisco...ma non le si torca un capello.
(I bravi vanno via e anche i due manigoldi ma arrivati a casa di Lucia hanno un’amara sorpresa)
Bravo (Ferrarese) chiama Don Rodrigo: Capo la pollastrella è volata via!!
Don Rodrigo: (bestemmia) Tornate qua, dobbiamo scoprire dove sono scappati!!

Scena V:
Inquadratura: autobus che parte, arrivo a Bolzano, separazione dei protagonisti.
Protagonisti: Lucia, Renzo, Agnese Suor Gertrude, studenti in rivolta, agenti in borghese
Narratore: 
Abbiamo lasciato i nostri sciagurati sull’autobus diretti a Bolzano. Arrivati in città, i tre si dividono, ma ahimè  Renzo, ragazzo ingenuo e sempre propenso a cacciarsi in situazioni poco piacevoli, finisce per ficcarsi  in  guai veramente grossi…  ma vediamo cosa succede.
Lucia si reca presso il convento della suora, e Renzo deve prendere l’autobus per Laives, Linea 110, come da indicazione dell’addetto alla biglietteria…ma le cose non vanno per il verso giusto…l’autobus è in ritardo, e Renzo dopo aver chiesto informazioni, su come arrivare a Laives, inizia a camminare a piedi…quando s’imbatte in una manifestazione studentesca, per curiosità si intrufola tra gli studenti che protestano.
Dialoghi:
Renzo: Scusa, cosa succede?
Studente: Protestiamo contro decisioni prese a discapito degli studenti, vogliono imporre il loro potere su di noi, ma noi non ci stiamo…vogliono toglierci la possibilità  di costruirci attivamente il nostro futuro…
Narratore:
…l’ingenuo Renzo, inizia a sbraitare contro i potenti, i delinquenti, urla frasi senza senso, incitando alla lotta…e per sua sfortuna, tra gli studenti vi sono agenti della polizia in borghese, avvisati della manifestazione.
Renzo: Facciamo sentire la nostra voce, basta con i soprusi da parte degli impuniti!!
Narratore:
Renzo viene circondato dai poliziotti e portato via, ma per sua fortuna, uno studente si accorge di quello che sta avvenendo e richiamando l’attenzione di altri studenti, accerchiano i poliziotti e Renzo fugge.
Studente: Ragazzi, venite qua aiutiamo il nostro amico!!
Narratore
… finalmente il nostro malcapitato riesce a prendere  l’autobus per Laives.

Scena VI:
Inquadratura: Lucia e la suora, rapimento e liberazione di Lucia, Lucia ascolta la radio, convento
Protagonisti: Don Rodrigo,  Innominato, Lucia, Suor Gertrude.
Narratore:
A Nova Levante, intanto, Don Rodrigo ha sguinzagliato i suoi uomini alla ricerca di Lucia, che sembra sparita nel nulla…quando alla radio parlano della manifestazione studentesca, sobillata da un certo Renzo Tramaglino, sospettato di far parte di uno dei centri sociali più estremisti, sfuggito all’arresto, ora è ricercato. Don Rodrigo, intanto, scopre per via traverse, che Lucia è a Bolzano, presso una suora di dubbia moralità. Don Rodrigo, allora, chiama un uomo, del quale nessuno conosce l’identità, ma la sua fama di uomo spietato è nota in tutto l’Alto Adige.
Costui si fa chiamare Innominato.
 Dialogo:
Don Rodrigo: ( effettua una video chiamata all’Innominato): Buonasera, la chiamo per aver un suo consiglio e aiuto, poiché mi trovo in un impegno impossibile.
Innominato: Ti ascolto, ma fa in fretta, ho altro da fare…
Don Rodrigo: Dovreste rapire per me una donna, tale Lucia Mondella, che ha trovato ricovero presso una suora a Bolzano 
Narratore: 
Brevemente Don Rodrigo, racconta i fatti, l’Innominato dopo aver ascoltato l’uomo… lo licenza con poche parole.
Innominato: …avevo sentito un pettegolezzo su questa storia!! Ritienila cosa già fatta…una sola cosa, devi mandarmi i tuoi uomini, i miei sono impegnati in affari ben più grossi…tra poco avrai da me l’avviso di quel che dovrai far…( e con un gesto della mano licenzia Don Rodrigo)

Narratore:
…intanto, Lucia è arrivata dalla suora, la quale le fa alcune domande, e soddisfatta dalle risposte di Lucia, decide di accogliere lei e la madre presso il suo convento. Ma la nostra Lucia non sa ancora che misero destino l’attende…infatti, l’Innominato chiama la suora e con lei mette in atto il rapimento: Lucia deve lasciare, con una scusa, il convento, sola senza la madre…e così Lucia esce nel tardo pomeriggio, e appena varcato il portone del convento… due loschi individui, appoggiati ad una macchina nera, si muovono verso di lei…la donna non riesce a scappare…poiché uno dei due l’afferra violentemente per un braccio


Lucia: ( si agita, urla): Bruto, lasciami andare, adesso chiamo il 113, 112…lasciami andare…( e continua a dimenarsi)

Narratore:
…la nostra eroina fu infilata con la forza nell’auto, che partì sgommando a tutta velocità…L’innominato aspettava Lucia a casa sua, aveva visto la foto su Facebook…e non le sembrava così bella da meritare tutto questo trambusto…valli a capire gli uomini, pensò stizzito lo stesso. Giunge l’auto e finalmente incontra la donna.
Innominato: Così saresti tu..la famosa Lucia… smettila di sbraitare come una gallina, non ti sente nessuno, e anche se questo potesse succedere nessuno avrebbe coraggio di mettersi contro di me.
Lucia: (messa da parte ogni spavalderia, inizia a piangere, supplicandolo di lasciarla andare): La prego non mi faccia del male, mi liberi, ora subito!!Vedo che lei a buon cuore…mi liberi, mi liberi!!
 Narratore:
…l’Innominato, di fronte a quelle parole, sentì qualcosa smuoversi dentro, era da un po’ che rifletteva sulla sua vita, da quando per caso su internet, aveva iniziato a leggere di Buddha…e questo stava mettendo in dubbio il suo operato…adesso gli occhi di questa innocente, le sue accorate preghiere lo stavano mettendo in crisi…passò una notte insonne, una frase di Buddha gli tornava insistentemente alla mente: “ perdona gli altri, non perché essi meritino perdono, ma perché tu meriti la pace” … restò tutta la notta in piedi a pensare e il mattino dopo...liberò Lucia.
Si chiederà, ora,  il nostro spettatore quanto tempo fosse trascorso dall’inizio del racconto, quasi tre giorni,  …solo, ma tanti erano stati gli avvenimenti, che sembrava trascorsa una vita.
Così Lucia, accompagnata dall’Innominato, raggiunse un albergo in città!! Arrivata all’hotel, Lucia chiede alla receptionist una stanza.
Dialoghi:
Lucia: Avete una stanza libera?
Receptionist: (Arianna): Certo, aspetti… ecco per lei la stanza n. 4. Mi dia i suoi documenti.

Narratore:
… intanto che la registrano,  prova chiamare Renzo, ma il cellulare è spento…Eh si Renzo, ha spento il cellulare… inoltre, adesso si fa chiamare Antonio Rivolta.



Scena VII:
Inquadratura: Lucia nella hall hotel Agnese la raggiunge…Renzo raggiunge Lucia
Protagonisti: Lucia, Agnese, Renzo e receptionist 

Narratore:
…la nostra storia sta per giungere al termine, infatti, Lucia chiama la madre, che la raggiunge all’hotel, insieme ritornano al paesello, ma di Renzo nessuna traccia…
E finalmente, trascorsi due anni dall’inizio dell’avventura,  Renzo che si macerava l’anima per la preoccupazione della sorte della sua dolce Lucia… decise di andarla a cercare…ma dove…decise di partire per il suo paese natale… e la condanna a suo carico, che fine aveva fatto? Ebbene, signor spettatore, gli inquirenti avevano appurato che il povero Renzo, non c’entrava nulla…ed egli aveva preso di tutto ciò dalla radio.
Renzo parte ed arriva al suo paese, e si dirige subito a casa di Lucia, la quale chiacchierava tranquillamente con un’amica (Bombacci).
Renzo: Lucia, amore mio….finalmente ti ho trovata ( urla dalla gioia Renzo)
Lucia (incredula, felice e piangendo, gli corre incontro): Amore mio….finalmente!! ( bacio)

Narratore:
…e bene mio caro spettatore… i nostri protagonisti possono finalmente convolare a giuste nozze…un momento e Don Rodrigo, che fine ha fatto???
Dopo, la conversione dell’Innominato, aveva preferito cambiare aria insieme ai suoi scagnozzi…onde evitare di pagare per i suoi innumerevoli crimini…
…e  vissero felici e contenti

The end
